package com.camba.app_killruddery;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.text.util.Linkify;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class AboutDialog extends Dialog
{
    private static Context mContext = null;

    public AboutDialog(Context context)
    {
        super(context);
        mContext = context;
    }

    /**
     * Standard Android on create method that gets called when the activity initialized.
     */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        setContentView(R.layout.aboutlayout);
        TextView tvLegal = (TextView) findViewById(R.id.legal_text);
        tvLegal.setText(Html.fromHtml(readRawTextFile(R.raw.legal)));
        TextView tvInfo = (TextView) findViewById(R.id.info_text);
        tvInfo.setText(Html.fromHtml(readRawTextFile(R.raw.info)));
        tvInfo.setLinkTextColor(Color.WHITE);
        Linkify.addLinks(tvInfo, Linkify.ALL);
        Linkify.addLinks(tvLegal, Linkify.ALL);
        Button btnClose = (Button) findViewById(R.id.btnclose);
        btnClose.setOnClickListener(new Button.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // TODO Auto-generated method stub
                AboutDialog.this.dismiss();
            }
        });
        ;
        ImageView iv = (ImageView) this.findViewById(R.id.ivshowlogoabout);
        Bitmap bmp = BitmapFactory.decodeResource(this.getContext().getResources(), R.drawable
                .ic_launcher);
        bmp = bmp.createScaledBitmap(bmp, bmp.getWidth() * 2, bmp.getHeight() * 2, false);
        iv.setImageBitmap(bmp);


    }


    public static String readRawTextFile(int id)
    {
        InputStream inputStream = mContext.getResources().openRawResource(id);
        InputStreamReader in = new InputStreamReader(inputStream);
        BufferedReader buf = new BufferedReader(in);
        String line;
        StringBuilder text = new StringBuilder();
        try
        {
            while((line = buf.readLine()) != null)
            {
                text.append(line);
            }
        }
        catch(IOException e)
        {
            return null;
        }
        return text.toString();
    }
}
