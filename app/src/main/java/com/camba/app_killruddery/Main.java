package com.camba.app_killruddery;


import android.app.Activity;
import android.app.AlertDialog;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidParameterException;


/**
 * Main activity mimics the audio streaming,
 * <p/>
 * instead of streaming audio data on network,
 * <p/>
 * it reads data from local file
 * <p/>
 * The main purpose is to show how to play data
 * <p/>
 * buffer in chunks with AudioTrack class
 */

public class Main extends Activity
{

    /**
     * Called when the activity is first created.
     */

    private static String LogTag = "sajjad";

    @Override

    public void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.mainjava);

        Button btnStatic = (Button) findViewById(R.id.btnStatic);

        btnStatic.setOnClickListener(new OnClickListener()
        {


            @Override

            public void onClick(View v)
            {

                PlayAudio(AudioTrack.MODE_STATIC);

            }

        });

        Button btnStream = (Button) findViewById(R.id.btnStream);

        btnStream.setOnClickListener(new OnClickListener()
        {


            @Override

            public void onClick(View v)
            {

                PlayAudio(AudioTrack.MODE_STREAM);


            }

        });


    }

    private void showDialog(String message)
    {
        AlertDialog ad = new AlertDialog.Builder(Main.this).create();
        ad.setMessage(message);
        ad.show();
    }

    private void PlayAudio(int mode)

    {

        if(AudioTrack.MODE_STATIC != mode && AudioTrack.MODE_STREAM != mode)

            throw new InvalidParameterException();

        Log.i(LogTag, "Valid arguments found. AudiTrack.Mode == Static is " + (AudioTrack.MODE_STATIC == mode));

        String audioFilePath = Environment.getExternalStorageDirectory() + "/test/audio.mp3";

        long fileSize = 0;

        long bytesWritten = 0;

        int bytesRead = 0;

        int bufferSize = 0;

        byte[] buffer;

        AudioTrack track;


        File audioFile = new File(audioFilePath);


        fileSize = audioFile.length();

        Log.i(LogTag, "File opened with lenght " + fileSize);


        if(AudioTrack.MODE_STREAM == mode)

        {

            bufferSize = 8000;

        }

        else

        {// AudioTrack.MODE_STATIC

            bufferSize = (int) fileSize;

        }

        buffer = new byte[bufferSize];

        Log.i(LogTag, "buffer size is " + buffer.length);

        track = new AudioTrack(AudioManager.STREAM_MUSIC, 44100,

                AudioFormat.CHANNEL_CONFIGURATION_STEREO, AudioFormat.ENCODING_DEFAULT,

                bufferSize, mode);
        //		track = new AudioTrack(AudioManager.STREAM_MUSIC, 44100, AudioFormat
        // .CHANNEL_CONFIGURATION_STEREO, AudioFormat.ENCODING_DEFAULT,  bufferSize, mode);

        Log.i(LogTag, "Track is " + track.toString());

        //in stream mode,

        //1. start track playback

        //2. write data to track

        if(AudioTrack.MODE_STREAM == mode)
        {
            Log.i(LogTag, "Going to play stream");
            track.play();
            Log.i(LogTag, "stream played");
        }

        FileInputStream audioStream = null;

        try
        {
            Log.i(LogTag, "Getting file input stream");
            audioStream = new FileInputStream(audioFile);

        }
        catch(FileNotFoundException e)
        {

            e.printStackTrace();

        }


        while(bytesWritten < fileSize)

        {

            try
            {
                Log.i(LogTag, "Reading buffer");
                bytesRead = audioStream.read(buffer, 0, bufferSize);
                Log.i(LogTag, "buffer reading done");

            }
            catch(IOException e)
            {

                //TODO Auto-generated catch block
                Log.i(LogTag, "Exception: " + e.toString() + "\r\n");
                e.printStackTrace();

            }
            Log.i(LogTag, "going to write bytes to buffer");
            bytesWritten += track.write(buffer, 0, bytesRead);

        }

        //in static mode,

        //1. write data to track

        //2. start track playback

        if(AudioTrack.MODE_STATIC == mode) Log.i(LogTag, "Going to play static mode");
        track.play();
        Log.i(LogTag, "static mode played");

    }

    @Override
    public void onStart()
    {
        super.onStart();

    }

    @Override
    public void onStop()
    {
        super.onStop();
    }

}