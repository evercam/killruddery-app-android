package com.camba.app_killruddery;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.view.Menu;

import com.bugsense.trace.BugSenseHandler;
import com.camba.app_killruddery.utils.Constants;
import com.google.analytics.tracking.android.EasyTracker;

public class preferncesActivity extends PreferenceActivity
{
    private static String LOGTAG = "activitypreferences";


    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(Constants.isAppTrackingEnabled)
        {
            BugSenseHandler.initAndStartSession(this, Constants.bugsense_ApiKey);
        }

        addPreferencesFromResource(R.layout.mypreferences);
        // Get the custom preference
        this.setDefaultKeyMode(DEFAULT_KEYS_DISABLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu m)
    {
        return false;
    }

    @Override
    public void onStart()
    {
        super.onStart();

        if(Constants.isAppTrackingEnabled)
        {
            EasyTracker.getInstance().activityStart(this);
            BugSenseHandler.startSession(this);
        }
    }

    @Override
    public void onStop()
    {
        super.onStop();
        if(Constants.isAppTrackingEnabled)
        {
            EasyTracker.getInstance().activityStop(this);
            BugSenseHandler.closeSession(this);
        }

    }


}