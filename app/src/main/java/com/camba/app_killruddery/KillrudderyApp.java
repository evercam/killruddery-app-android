package com.camba.app_killruddery;

import android.app.Application;

import org.acra.ACRA;

public class KillrudderyApp extends Application
{

    @Override
    public void onCreate()
    {
        super.onCreate();

        // The following line triggers the initialization of ACRA
        ACRA.init(this);
    }

}
