package com.camba.app_killruddery;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bugsense.trace.BugSenseHandler;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;

//import android.app.ActionBar.LayoutParams;


public class KillrudderyLiveViewActivity extends Activity implements OnPreparedListener
{

    private final static String LOGTAG = "KillrudderyActivity";
    private int screen_width, screen_height;
    private int media_width = 0, media_height = 0;
    private boolean landscape;
    protected PowerManager.WakeLock mWakeLock;
    private RelativeLayout iView;
    private ImageView ivMediaPlayer;
    public String imageURL = "http://killruddery.evercam.camera:8001/snapshot.jpg";
    public String audioURL = "rtsp://killruddery.evercam.camera:8001/live_amr_audio.sdp";
    public long downloadStartCount = 0;
    public long downloadEndCount = 0;
    public TextView txtframerate;
    public Thread imageThread;

    public MediaPlayer mp;

    public int sleepIntervalStartTime = 51;
    public int intervalAdjustment = 10;
    public int sleepInterval = sleepIntervalStartTime + 450;
    public boolean startDownloading = false;
    private static long latestStartImageTime = 0;

    //	private static int successiveFailureCount = 0;

    private Boolean optionsActivityStarted = false;

    public AlertDialog adLocalNetwork;

    private static String host = "killruddery.evercam.camera", port = "8001", audioRelativeURL =
            "live_amr_audio.sdp", HighResImageURL = "snapshot.jpg", LowResImageURL =
            "snapshot_3gp.jpg";

    Boolean playAudio = true;
    boolean chkhighqualityvideo = true;
    Boolean chkenablocalnetwork = false;
    String localip = "192.168.1.234";
    String localport = "80";
    boolean paused = false;

    Animation myFadeInAnimation = null;


    public boolean isOnline()
    {
        try
        {
            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context
                    .CONNECTIVITY_SERVICE);

            return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo()
                    .isConnectedOrConnecting();
        }
        catch(Exception ex)
        {
            Log.e(LOGTAG, ex.toString());
        }
        return false;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        try
        {
            //			android.R.drawable.ic_menu_info_details
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.layout.menulayout, menu);
            return true;
        }
        catch(Exception ex)
        {
            Log.e(LOGTAG, ex.toString());
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle item selection
        switch(item.getItemId())
        {
            case R.id.menusettings:
                paused = true;
                startActivity(new Intent(this, preferncesActivity.class));
                optionsActivityStarted = true;
                Log.i(LOGTAG, "Options Activity Started in onPrepareOptionsMenu event");
                return true;
            case R.id.menuabout:
                AboutDialog about = new AboutDialog(this);
                //			about.setTitle("About Application");
                about.setCancelable(true);
                about.requestWindowFeature(Window.FEATURE_NO_TITLE);
                about.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void readSetPreferences()
    {
        try
        {

            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);

            Boolean bchkenablocalnetwork = false;
            Boolean bchkenabfps = false;

            playAudio = sharedPrefs.getBoolean("chkenableaudio", true);


            chkhighqualityvideo = sharedPrefs.getBoolean("chkhighqualityvideo", true);
            bchkenabfps = sharedPrefs.getBoolean("chkenablefps", false);


            if(chkenablocalnetwork != sharedPrefs.getBoolean("chkenablocalnetwork", false))
            {
                chkenablocalnetwork = sharedPrefs.getBoolean("chkenablocalnetwork", false);
                bchkenablocalnetwork = true;
            }


            Log.i(LOGTAG, "playAudio:" + playAudio);
            Log.i(LOGTAG, "chkhighqualityvideo:" + chkhighqualityvideo);
            Log.i(LOGTAG, "chkenablocalnetwork:" + chkenablocalnetwork);

            if(chkenablocalnetwork)
            {
                imageURL = "http://" + localip + ":" + localport + "/" + (chkhighqualityvideo ?
                        HighResImageURL : LowResImageURL);
                audioURL = "rtsp://" + localip + ":" + localport + "/" + audioRelativeURL;
                taskCheckReachableIP task = new taskCheckReachableIP();
                task.execute(localip);
            }
            else
            {
                imageURL = "http://" + host + ":" + port + "/" + (chkhighqualityvideo ?
                        HighResImageURL : LowResImageURL);
                audioURL = "rtsp://" + host + ":" + port + "/" + audioRelativeURL;
            }


            Log.i(LOGTAG, "imageURL:" + imageURL);
            Log.i(LOGTAG, "audioURL:" + audioURL);

            if(playAudio && ((mp == null || !mp.isPlaying()) || (bchkenablocalnetwork)))
            {
                Log.i(LOGTAG, "Going to start player so that user can listen the audio of " +
                        "camera:" + audioURL);
                StartAudioPlayer();
            }
            else if(!playAudio)
            {
                Log.i(LOGTAG, "Going to stop player so that user can't listen the audio of " +
                        "camera:" + audioURL);
                StopAudioPlayer();
            }

            if(bchkenabfps) txtframerate.setVisibility(View.VISIBLE);
            else txtframerate.setVisibility(View.GONE);

        }
        catch(Exception ex)
        {
            Log.e(LOGTAG, ex.toString());
        }
    }

    public void StartAudioPlayer()
    {
        try
        {
            if(mp != null)
            {
                mp.release();
                mp = null;
            }
            mp = new MediaPlayer();
            mp.setDataSource(audioURL);
            mp.setOnPreparedListener(null);
            mp.setOnPreparedListener(this);
            mp.prepareAsync();
            Log.i(LOGTAG, "audio player prepare async method called." + audioURL);

        }
        catch(Exception ex)
        {
            Log.e(LOGTAG, ex.toString());
            return;
        }

    }

    public void StopAudioPlayer()
    {
        try
        {
            if(mp != null) mp.stop();
            mp.release();
            mp = null;
            Log.i(LOGTAG, "Audio player stopped");
        }
        catch(Exception ex)
        {
            Log.e(LOGTAG, ex.toString());
            return;
        }

    }


    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        try
        {


            Log.i(LOGTAG, "settign imagelayout");
            setContentView(R.layout.imagelayout);


            if(!isOnline())
            {
                try
                {
                    AlertDialog ad = new AlertDialog.Builder(KillrudderyLiveViewActivity.this)
                            .create();
                    ad.setTitle("Network not available");
                    ad.setMessage("Please connect to internet and try again.");
                    ad.setButton(AlertDialog.BUTTON1, "Close", new DialogInterface.OnClickListener()
                    {

                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            // TODO Auto-generated method stub
                            dialog.dismiss();
                            KillrudderyLiveViewActivity.this.onStop();
                        }
                    });
                    ad.show();
                    return;
                }
                catch(Exception ex)
                {
                }
            }


            Log.i(LOGTAG, "getting Image View");
            iView = (RelativeLayout) this.findViewById(R.id.camimage);
            ivMediaPlayer = (ImageView) this.findViewById(R.id.ivmediaplayer);
            txtframerate = (TextView) KillrudderyLiveViewActivity.this.findViewById(R.id
                    .txtframerate);


            readSetPreferences();


            iView.setOnClickListener(new OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(paused)
                    {
                        resumeVideoAudio();
                        //						Toast.makeText(getApplicationContext(), "Played",
                        // 2500).show();
                        ivMediaPlayer.setImageBitmap(null);
                        ivMediaPlayer.setVisibility(View.VISIBLE);
                        ivMediaPlayer.setImageResource(android.R.drawable.ic_media_pause);
                        myFadeInAnimation = AnimationUtils.loadAnimation
                                (KillrudderyLiveViewActivity.this, R.layout.fadein);
                        myFadeInAnimation.setAnimationListener(new Animation.AnimationListener()
                        {
                            @Override
                            public void onAnimationStart(Animation animation)
                            {
                                // TODO Auto-generated method stub
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation)
                            {
                                // TODO Auto-generated method stub
                            }

                            @Override
                            public void onAnimationEnd(Animation animation)
                            {
                                ivMediaPlayer.setVisibility(View.GONE);

                            }
                        });

                        ivMediaPlayer.startAnimation(myFadeInAnimation);

                        paused = false;
                    }
                    else
                    {
                        pauseVideoAudio();

                        ivMediaPlayer.clearAnimation();
                        if(myFadeInAnimation != null && myFadeInAnimation.hasStarted())
                        {
                            myFadeInAnimation.cancel();
                            myFadeInAnimation.reset();

                        }
                        ivMediaPlayer.setVisibility(View.VISIBLE);
                        ivMediaPlayer.setImageResource(android.R.drawable.ic_media_play);


                        paused = true;
                    }

                }
            });
            Log.i(LOGTAG, "Got image view " + iView.toString());

            // Get the size of the device, will be our maximum.
            Display display = getWindowManager().getDefaultDisplay();
            screen_width = display.getWidth();
            screen_height = display.getHeight();
            Log.i(LOGTAG, "Got Display specs");

            final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
            this.mWakeLock = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK, "My Tag");
            this.mWakeLock.acquire();
            Log.i(LOGTAG, "acquired the power lock");

            imageThread = new Thread(new Runnable()
            {
                @Override
                public void run()
                {
                    // TODO Auto-generated method stub
                    txtframerate = (TextView) KillrudderyLiveViewActivity.this.findViewById(R.id
                            .txtframerate);


                    boolean end = false;
                    while(!end)
                    {
                        try
                        {
                            // wait for starting
                            try
                            {
                                while(!startDownloading)
                                {
                                    Log.i(LOGTAG, "going to sleep for half second.");
                                    ;
                                    Thread.sleep(500);
                                }
                            }
                            catch(Exception e)
                            {
                            }

                            downloadStartCount++;
                            Log.i(LOGTAG, "Starting download image tag.");
                            DownloadImage task = new DownloadImage();
                            if(android.os.Build.VERSION.SDK_INT >= 11)
                                task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, imageURL);
                            else task.execute(new String[]{imageURL});

                            if(downloadStartCount - downloadEndCount > 9)
                            {
                                sleepInterval += intervalAdjustment;
                            }
                            else if(sleepInterval >= sleepIntervalStartTime)
                            {
                                sleepInterval -= intervalAdjustment;
                            }

                            Thread.sleep(sleepInterval, 0);

                        }
                        catch(Exception ex)
                        {
                            downloadStartCount--;
                            Log.e(LOGTAG, ex.toString());
                            BugSenseHandler.sendException(ex);
                        }
                    }
                }
            });
            if(!this.paused)
            {
                startDownloading = false;
                DownloadImage task = new DownloadImage();
                if(android.os.Build.VERSION.SDK_INT >= 11)
                    task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, imageURL);
                else task.execute(new String[]{imageURL});
                Log.i(LOGTAG, "Task execution started for first image");
                imageThread.start();
                Log.i(LOGTAG, "Image Thread Started");
            }

        }
        catch(Exception ex)
        {
            Log.e(LOGTAG, ex.toString(), ex);
            BugSenseHandler.sendException(ex);
        }
    }

    @Override
    public void onResume()
    {
        try
        {
            super.onResume();
            Log.i(LOGTAG, "onResume called");
            if(optionsActivityStarted)
            {
                optionsActivityStarted = false;
                this.paused = false;
                Log.i(LOGTAG, "onResume in block executed");
                readSetPreferences();
                latestStartImageTime = SystemClock.uptimeMillis();

                startDownloading = false;

                DownloadImage task = new DownloadImage();
                if(android.os.Build.VERSION.SDK_INT >= 11)
                    task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, imageURL);
                else task.execute(new String[]{imageURL});


                if(!imageThread.isAlive())
                {
                    imageThread.start();
                }
            }
        }
        catch(Exception ex)
        {
            Log.e(LOGTAG, ex.toString());
            BugSenseHandler.sendException(ex);
        }
    }

    @Override
    public void onRestart()
    {
        try
        {
            super.onRestart();
            Log.i(LOGTAG, "onRestart called");
            if(optionsActivityStarted)
            {
                optionsActivityStarted = false;
                this.paused = false;
                Log.i(LOGTAG, "onRestart in block executed");
                readSetPreferences();
                latestStartImageTime = SystemClock.uptimeMillis();

                startDownloading = false;

                DownloadImage task = new DownloadImage();
                if(android.os.Build.VERSION.SDK_INT >= 11)
                    task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, imageURL);
                else task.execute(new String[]{imageURL});


                if(!imageThread.isAlive())
                {
                    imageThread.start();
                }
            }
        }
        catch(Exception ex)
        {
            Log.e(LOGTAG, ex.toString());
            BugSenseHandler.sendException(ex);
        }
    }

    public void pauseVideoAudio()
    {
        try
        {
            if(mp != null && mp.isPlaying()) mp.pause();
        }
        catch(Exception ex)
        {
            Log.e(LOGTAG, ex.toString());
            BugSenseHandler.sendException(ex);
        }
    }

    public void resumeVideoAudio()
    {
        try
        {
            if(mp != null) mp.start();
        }
        catch(Exception ex)
        {
            Log.e(LOGTAG, ex.toString());
            BugSenseHandler.sendException(ex);
        }
    }


    @Override
    public void onPause()
    {
        try
        {
            super.onPause();
            Log.i(LOGTAG, "onPause called");
            if(!optionsActivityStarted)
            {
                this.onDestroy();
                Log.i(LOGTAG, "onPause in block executed");
                android.os.Process.killProcess(android.os.Process.myPid());
                System.runFinalizersOnExit(true);
                System.exit(0);

                super.finish();
                this.finish();

                Log.i(LOGTAG, "goint to end the " +
                        "activity......................................................................................");
            }
        }
        catch(Exception ex)
        {
        }
    }


    @Override
    public void onStart()
    {
        super.onStart();
    }

    @Override
    public void onStop()
    {
        try
        {
            super.onStop();
            if(!optionsActivityStarted)
            {
                Log.i(LOGTAG, "onStop in block executed");
                this.onDestroy();
                android.os.Process.killProcess(android.os.Process.myPid());
                System.runFinalizersOnExit(true);
                System.exit(0);

                super.finish();
                this.finish();
            }
        }
        catch(Exception ex)
        {
        }


    }

    @Override
    public void onDestroy()
    {
        try
        {
            super.onDestroy();
            if(this.mWakeLock != null) this.mWakeLock.release();
            if(mp != null)
            {
                if(mp.isPlaying()) mp.stop();
                mp.release();
                mp = null;
            }
        }
        catch(Exception ex)
        {
        }

    }

    /**
     * The user rotated the screen.
     * Since we specified in the AndroidManifest.xml that we want to handle our
     * own orientation changes, we resize the screen in function of being
     * portrait or landscape.
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        try
        {
            super.onConfigurationChanged(newConfig);
            landscape = (Configuration.ORIENTATION_LANDSCAPE == newConfig.orientation);
            //resize();
        }
        catch(Exception e)
        {
        }
    }

    /**
     * Resizes the surfaces to fill the screen.
     * If the media_width and media_height variables are zero, that means we
     * don't know yet the size of the video, so we just resize to fill the
     * whole screen. Otherwise we scale maintaining the aspect ratio to fill
     * it. The gravity of the layout leaves us centered.
     */
    public void resize(int imageHieght, int imageWidth)
    {
        int w = landscape ? screen_height : screen_width;
        int h = landscape ? screen_width : screen_height;

        // If we have the media, calculate best scaling inside bounds.
        if(imageWidth > 0 && imageHieght > 0)
        {
            final float max_w = w;
            final float max_h = h;
            float temp_w = imageWidth;
            float temp_h = imageHieght;
            float factor = max_w / temp_w;
            temp_w *= factor;
            temp_h *= factor;

            // If we went above the height limit, scale down.
            if(temp_h > max_h)
            {
                factor = max_h / temp_h;
                temp_w *= factor;
                temp_h *= factor;
            }

            w = (int) temp_w;
            h = (int) temp_h;
        }
        media_height = (int) h;
        media_width = (int) w;
        Log.i(LOGTAG, "resize method called: " + w + ":" + h);
    }

    public static Bitmap DownlaodImageSync(URL url) throws IOException
    {
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoInput(true);
        connection.connect();
        InputStream input = connection.getInputStream();
        return BitmapFactory.decodeStream(input);
    }

    //	/** The media player is prepared.
    //	 * Sometimes the media player doesn't have yet the sizes of the video. If
    //	 * that is the case, register a video size change listener, otherwise
    //	 * resize ourselves and start the video.
    //	 */
    //
    private class DownloadImage extends AsyncTask<String, Void, Bitmap>
    {
        private long myStartImageTime;

        @Override
        protected Bitmap doInBackground(String... urls)
        {
            Bitmap response = null;
            for(String url1 : urls)
            {
                try
                {
                    myStartImageTime = SystemClock.uptimeMillis();
                    URL url = new URL(url1);
                    Log.i(LOGTAG, "Going to download image");
                    response = DownlaodImageSync(url);
                    Log.i(LOGTAG, "Downloaded image");
                    //					successiveFailureCount = 0;
                }
                catch(Exception e)
                {
                    Log.e(LOGTAG, "Exception: " + e.toString());
                    BugSenseHandler.sendException(e);
                    //					+ (++successiveFailureCount)
                }
            }
            return response;
        }

        @Override
        protected void onPostExecute(Bitmap result)
        {
            try
            {
                startDownloading = true;
                downloadEndCount++;
                if(result != null && result.getWidth() > 0 && result.getHeight() > 0 && myStartImageTime > latestStartImageTime && !paused)
                {
                    resize(result.getHeight(), result.getWidth());
                    Log.i(LOGTAG, "sw sh [" + screen_width + "x" + screen_height + "], mw mh  [" + media_width + "x" + media_height + "], iw ih  [" + result.getWidth() + "x" + result.getHeight() + "]");
                    latestStartImageTime = myStartImageTime;
                    Bitmap tmp = Bitmap.createScaledBitmap(result, media_width, media_height, true);
                    //					iView.setImageBitmap(tmp);
                    Log.i(LOGTAG, "sw sh [" + screen_width + "x" + screen_height + "], mw mh  [" + media_width + "x" + media_height + "], iw ih  [" + tmp.getWidth() + "x" + tmp.getHeight() + "]");
                    iView.getLayoutParams().height = tmp.getHeight();
                    iView.getLayoutParams().width = tmp.getWidth();
                    Drawable dr = new BitmapDrawable(tmp);
                    iView.setBackgroundDrawable(dr);
                    txtframerate.setText(String.format("[F/s:%.2f]", (1000 / (float) sleepInterval)));

                    Log.i(LOGTAG, "downloaded an image: " + media_width + ":" + media_height);
                }
                else
                {
                    Log.i(LOGTAG, "downloaded image discarded. ");
                }
            }
            catch(Exception e)
            {
                Log.e(LOGTAG, e.toString());
                BugSenseHandler.sendException(e);
            }
        }
    }

    @Override
    public void onPrepared(MediaPlayer _mp)
    {
        try
        {
            _mp.start();
            Log.i(LOGTAG, "Player started in on prepared Method");
        }
        catch(Exception e)
        {
            Log.e(LOGTAG, e.toString());
            BugSenseHandler.sendException(e);
        }
        // TODO Auto-generated method stub

    }


    private class taskCheckReachableIP extends AsyncTask<String, Void, Boolean>
    {
        @Override
        protected Boolean doInBackground(String... IPs)
        {
            for(String ip : IPs)
            {
                try
                {
                    return InetAddress.getByName("GOOGLE.COM").isReachable(1000 * 5);
                }
                catch(Exception e)
                {
                    Log.e(LOGTAG, "Exception: " + e.toString());
                    BugSenseHandler.sendException(e);
                }
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean result)
        {
            try
            {
                adLocalNetwork = new AlertDialog.Builder(KillrudderyLiveViewActivity.this).create();
                //				adLocalNetwork.setTitle("Local Network In-Accessible");
                adLocalNetwork.setMessage("Stream not found. In settings, try turning on/off local network.");
                adLocalNetwork.setButton(AlertDialog.BUTTON1, "Close", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });

                adLocalNetwork.show();
                StopAudioPlayer();
                paused = true;

            }
            catch(Exception e)
            {
                Log.e(LOGTAG, e.toString());
                BugSenseHandler.sendException(e);
            }
        }
    }


}